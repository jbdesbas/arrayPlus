# ArrayPlus

This QGIS plugin add some functions to the field editor for manipulate arrays (list) and maps (associative array).
The hstore_to_map function can be usefull for handle osm files with "other_tags" field.

⚠️ Since **QGIS 3.20**, all functions have been integrated into Qgis core, so this plugin is depreciated. For *array_lambda* and *array_avg*, please use *array_foreach* and *array_mean*.

## Funtions :

### Array

- array_min
- array_max
- array_minority
- array_majority
- array_count
- array_avg
- array_sum
- array_sort
- array_replace
- array_lambda

### Map
- hstore_to_map
- json_to_map
